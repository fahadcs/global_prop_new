;(function($) {
/**
 *	
 *	Description: Transforms input tags into ajax-upload images.
 *	
 *	Required input: <input type="text" id="uniq-id" data-img-src="image_url_here" data-upload-folder="folder_to_upload_to" data-dashboard-url="dashboard_url_here" />
 *	Sample Usage: $('#uniq-id').imgupload();
 *
 */
$.fn.imgupload = function()
{

	var img_input = this;
	var id = this.attr('id');
	var image_holder = this.parent();
	var img_src = this.data('img-src');
	var dashboard_url = this.data('dashboard-url');
	var upload_folder = this.data('upload-folder');
	var prev_img = '';
	
	this.css('display', 'none');
	image_holder.html(img_input);
	if( $('#'+id+'-upload-btn').length ==0 )
	this.parent().append(
		'<div style="display:inline-block;text-align:left;" class="image-holder">'+
		'<a class="btn btn-small btn-success" id="'+id+'-upload-btn" href="javascript:;" style="position:absolute;margin-top:10px;margin-left:10px;display:none;">Upload Image</a>'+
		'<img class="img" src="'+img_src+'" style="width:'+image_holder.css('width')+';" />'+
		'</div>'
	);
	$('#'+id+'-upload-btn').show();
	if( $('#'+id+'-img-upload').length ==0 )
	this.closest('form').parent().append(
		'<form style="display:none" id="'+id+'-img-upload" class="ajax-img-upload-form" action="'+dashboard_url+'imgupload/nd_upload" target="'+id+'-iframe-post-form" method="post" enctype="multipart/form-data">'+
			'<input id="'+id+'-img-input" name="userfile" type="file" class="btn" style="display:none" accept="image/jpg, image/jpeg, image/png, image/bmp"/>'+
			'<input type="hidden" name="upload_path" value="'+upload_folder+'" />'+
		'</form>'+
		'<iframe id="'+id+'-iframe-post-form" name="'+id+'-iframe-post-form" style="display:none"></iframe>'
	);
	/*
	if( $('#'+id+'-img-upload').length ==0 )
	this.parent().append(
		'<form style="display:none" id="'+id+'-img-upload" action="'+dashboard_url+'imgupload/nd_upload" target="'+id+'-iframe-post-form" method="post" enctype="multipart/form-data">'+
			'<input id="'+id+'-img-input" name="userfile" type="file" class="btn" style="display:none" accept="image/jpg, image/jpeg, image/png, image/gif, image/bmp"/>'+
			'<input type="hidden" name="upload_path" value="'+upload_folder+'" />'+
		'</form>'+
		'<iframe id="'+id+'-iframe-post-form" name="'+id+'-iframe-post-form" style="display:none"></iframe>'
	);
	*/
	this.val( image_holder.find('.img').attr("src") );
	
	image_holder.find('.img').css('display','block');
	
	image_holder.find('div').mouseover(function() {
		$('#'+id+'-upload-btn').show();
	});
	
	image_holder.find('div').mouseout(function() {
		$('#'+id+'-upload-btn').hide();
	});
	
	$('#'+id+'-upload-btn').click(function() {
		$('#'+id+'-img-input').click();
	});
	
	$('#'+id+'-img-input').change(function() {
		if($('#'+id+'-img-input').val()!=''){
			prev_img = image_holder.find('.img');
			//image_holder.find('.img').remove();
			//image_holder.append('<img class="imgupload-loader" src="img/ajax-loader-48.gif" style="display:block"/>');

			if (this.files && this.files[0]) {
				var reader = new FileReader();
				//2457600 300kb
				if(this.files[0].size > 6144000){//1Mb
					alert('Selected file exceeded file size limit: 750kB..');
				}else{
					reader.onload = function (e) {
						image_holder.find('.img').attr('src', e.target.result);
					}

					reader.readAsDataURL(this.files[0]);
				}
			}
			
			// $('#'+id+'-img-upload').serialize();
			// $('#'+id+'-img-upload').submit();
		}
	});
	
	// $('#'+id+'-iframe-post-form').load(function() {
		// var a = $('#'+id+'-iframe-post-form').contents().find("#upload_status").html();  
		// if (a) {
			// var obj = jQuery.parseJSON(a);    
				// if (obj.status == 'success') { 
					// image_holder.find('.imgupload-loader').remove();
					// image_holder.find('div').append('<img src="../../upload/'+upload_folder+''+obj.file_name+'" alt="Uploading...."id="img_preview" class="img"/>'); 
					// img_input.val("../../upload/"+upload_folder+""+obj.file_name);
					// image_holder.find('.img').css('display','inline');
				// } // #end success 
				// else if(obj.status == 'error'){
					// alert(obj.error_message); 
					// image_holder.find('.imgupload-loader').remove();
					// image_holder.find('div').append(prev_img);
				// }
				// else{
					// image_holder.html('Upload Failed. Please try again...'); 
				// }
		// } // #end a!=null  
	// }); // #end upload_hidden load  

}
	
})(jQuery);