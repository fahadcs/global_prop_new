<?php include('header.php') ?>

<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Our Referral Program</li>
              </ol>
              <h2>Our Referral Program</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->

    <section class="content-wrapper Our-Referral-Program">
        <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <img class="img-fluid" src="http://placehold.jp/1200x500.png" alt="">
                  <br><br>
                  <p>Of course we would like for you to referr other investors to GLOBALPROP, and we wish to reward you for doing so. If another investor pledges and confirms funds using your referral code, the amount they pledge will be added to your referral total. Whenever your referral total reaches $10,000.00 we will increase your precentage earnings by 1%. For example, say you have $499.00 invested yourself, and you are expecting a return of 11% on that money. But you also have $20,000.00 in referral reward money that was pledged by people who you invited into the program. For each $10,000.00 referral reward money in your account, the interest on your $499,00 will increase by 1%, giving you a new ROI of 13% instead of the initial 11%.</p>
                  <p>The same is true if you invest $100,000.00 and recieve 8% initial ROI, but we have to cap rewards at 13%. Imagine what would happen otherwise. If your referral rewards money reaches an additional $100,000.00, and we paid you an extra 10% based on that, your $100,000.00 pledge would reach 18% RIO, which would strain the project. Putting a cap on the rewards program will still yield you an overall ROI of 13% on your $100,000.00 (an additional 5%), and at the same time does not unduley burden the project.</p>          
              </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.project-lists -->



<?php include('footer.php') ?>