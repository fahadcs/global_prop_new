<?php include('header.php'); ?>
<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url();?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Project Details</li>
              </ol>
              <h2><?php echo $proj['title']; ?></h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->


    <section class="project-details">
        <div class="container">
            <?php
            
            echo "<div id='container'>";
            
            $msg2= "";  $msg3= "";
            //we must check if the user is an investor and only add the invest table/form if it is
            if($usertype== 'investor'){	
              if($proj['projAmt'] > $proj['amtFunded'] || $inv['confirm']== 0){
                    //Check confirm switch and if it is 1, set it back to 0
                    //check if this is an edit of an existing pledge or a new pledge and change the messaging accordingly
                    if($inv['amt']== 0){
                        $form_data1= array(
                                'name'=>'refNum',
                                'placeholder'=>'referral code here',
                        );
                        $msg1= 'You have no investment yet.';
                        $msg2= "<span style='font-size:.8em'>You can add your friend's referral number only the first time you 
                        pledge on a project. The amount you pledge will go towards your friend's rewards total and work 
                        to increase their ROI. <br/> <b><u>If you invite friends and they invest, your ROI can go up too.</u></b></style>";			
                        $refCode= "<td>".form_label('Referral', 'referral').form_input($form_data1)."</td>";
                    }else{
                        
                        if($refTot > 0){
                          $msg3= "Good News!!<br/><u>Someone Used Your Ref #</u><br/>Your Rewards: $".$refTot.".00";
                        }else{
                            $msg3= "Sorry...<br/><u>No one has Used Your Ref # yet</u><br/>Your Rewards: $".$refTot.".00";
                        }//end if $refTot > 0
                          $msg1= 'Your amount in USD: $'.$inv['amt'].'.00';	
                          $msg2= "<span style='font-size:.8em'>You have already invested in this project, but you can increase your overall
                                  RIO if you invite your friends to invest here. <br/>
                                  <b><u>Make more when your friends invest in a project that you've funded.</u></b></style>";
                         //Check if the total amount funded is greater than the total amount needed and add confirm button
                         if($proj['projAmt'] <= $proj['amtFunded']){
                             if($inv['confirm']== 0){
                               $msg2= "<div style='background:#FFAEB5; width:150px;'><a href='".base_url().
                               "invests/confirm_pledges/".$proj['projID']."'>&nbsp;<b>Confirm Your Pledge</b></div></a><br/>
                               Please confirm your pledge so that the project can start. 
                               The Amount Pledged has exceeded the Amount Needed.<br/>";
                             }else{
                                 $msg2= "<div style='background:#90FFA7;'><center>Congratulations! Your pledge is confirmed.</center>.</div>";
                             }//end if $inv['confirm']== 0
                         }else{
                         }//end if $proj['projAmt'] <= $proj['amtFunded']			  
                          $refCode= "<td><center>".$msg3."</center></td>".form_hidden('refNum',0);		
                    }//end if $inv[amt]== 0
                    
                    
                    
                    $form_data2= array(
                        'name'=>'invest',
                        'placeholder'=>$inv['amt'],
                    );
                    
                    //if the user is an investor $td2 var outputs the invest table/form 
                    $td2= "<table>".form_open(base_url().'invests/new_invest')."
                          <tr>".$refCode.		  
                            "<td>Your Referral #<br/> &nbsp; &nbsp; ** ".
                            $userRef." **<br/>&nbsp; Invite a Friend
                          </td>
                          </tr>
                          <tr><td>".
                            form_input($form_data2). form_hidden('projID',$proj['projID']). form_hidden('amtFunded',$proj['amtFunded']).
                            "<br/><div style='background:green; color:white; width:220px;'>
                                <center>".$msg1."</center>
                             </div></td><td>".form_submit('','Change Amount').
                           "</td</tr></table>"
                          .form_close();	
                  }else{
                      $td2= "<div style='background:red; color:white; width:220px;'>Investment is Currently Closed</div><br/>
                               <div style='background:green; color:white; width:220px;'>
                                <center>Your amount in USD: $".$inv['amt'].".00</center>
                             </div>";
                  }//end if projAmt is greater than amtFunded
            }else{
                //if the user is not an investor $td2 outputs a message instead of the invest table/form
                $td2= "";
            }//end if $usertype== investor
            
                    echo "<table class='table'><tr>
                <td><h1>Project Details</h1>".$msg2."</td>
                <td>".$td2."</td>
                </tr></table>";
            ?>
            <?php
                if($usertype != 'investor'){
            ?>
            <div class="row meesage-investors">
              <div class="col-lg-12">
                <div class="alert alert-success" role="alert">
                  You must be logged in with an investor's account in order to invest in any projects.
                </div>
              </div>
            </div> 
            <?php  } ?>           
            <div class="card project-details-top">
              <h2><?php echo $proj['title']; ?></h2>
              <p class="map"><i class="fas fa-map-marker"></i> 398 Pete Pascale Pl, New York</p>
              <div class="row">
                <div class="col-lg-4">
                  <p>Project Posted: <span><?php echo $proj['date_added']; ?></span></p>
                </div>
                <div class="col-lg-4">
                  <p>Project Total: <span><?php echo $proj['projAmt']; ?>.00</span></p>
                </div>
                <div class="col-lg-4">
                  <p>Amount Funded so-far: <span><?php echo $proj['amtFunded']; ?>.00</span></p>
                </div>
              </div><!-- /.row -->
              <div class="des">
                <?php echo $proj['descr']; ?>
              </div>
              <?php
                    if(isset($invList)){
              ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Pledge</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                        foreach($invList as $row ){
                      ?>
                        <tr>                   
                        <td><?php echo $row['username']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td class="text-primary"><?php echo $row['amt']; ?>.00</td>
                        </tr>
                    <?php } ?>
                  </tbody>
                </table><!-- /.table -->
              </div>
            
              <?php } ?>

              <div class="property-gallery">
                <div class="row property-gallery-heading">
                  <div class="col-md-12">
                    <h2>Property Gallery</h2>
                  </div>                  
                </div><!-- /.property-gallery-heading -->
                <div class="row popup-gallery">
                    <?php if ( !empty($proj_images) ) {
                            foreach ( $proj_images as $proj_img ) {
                        ?>
                        <div class="col-md-4">
                        <a href="<?php echo base_url($proj_img['full_path']); ?>" data-lightbox="project-<?php echo $proj['projID']; ?>"><img style="width:326px; height:217px;" class="img-fluid" src="<?php echo base_url($proj_img['full_path']); ?>" alt=""></a>
                        </div>

                   <?php } } ?>
                    
                </div><!-- /.row -->
              </div><!-- /.property-gallery -->
              <div class="property-gallery">
                <div class="row property-gallery-heading">
                  <div class="col-md-12">
                    <h2>Property Video</h2>
                  </div>                  
                </div><!-- /.property-gallery-heading -->
               <!--  <div class="row">
                  <div class="col-lg-12">
                    <video autoplay controls>
                      <source src="property.mp4" type="video/mp4">
                      <source src="movie.ogg" type="video/ogg">
                      Your browser does not support the video tag.
                    </video>
                  </div>                 
                </div> -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                    </div>
                  </div>                 
                </div>
              </div><!-- /.property-gallery -->
            </div><!-- /.card -->                        
        </div><!-- /.container -->
    </section><!-- /.project-lists -->

<?php include('footer.php'); ?>