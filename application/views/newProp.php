<?php include('header.php');
	echo "<h1>Create New Projects</h1>";
	$data_form1=array(
        'name'=>'tit',
    );
    $data_form2=array(
        'name'=>'desc',
    );
    $data_form3=array(
        'name'=>'val',
    );
    $data_form4=array(
        'userID'=>$userID,
    );
      
?>
    <script src="<?php echo base_url("assets/js/jquery.form.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload.js?v=5"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload2.js?5"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js?5"); ?>"></script>
    
<?php
	echo form_open(base_url().'props/newProp');
	echo "<table class='table'>";
	echo "<tr><td>".form_label('Project Title', 'title')."</td><td>".form_input($data_form1)."</td></tr>";
?>
    <tr>
        <td style="width:17%;">
            <label for="descr">Description</label>
        </td>
        <td>
            <div style="display:inline-block;">
                <textarea class="ckeditor" name="<?= $data_form2['name']; ?>" id="descr" cols="40" rows="10"></textarea>
            </div>
        </td>
    </tr>
<?php
	echo "<tr><td>".form_label('Project Value', 'value')."</td><td>".form_input($data_form3)."</td></tr>";
?>
    <tr>
        <td>
            <label for="">Images</label>
        </td>
        <td id="proj_images_cell" style="max-width:200px;">
            <div style="margin-bottom:10px;"><button class="btn btn-default" id="add_proj_img" type="button">Add Image</button></div>
        </td>
    </tr>
<?php
	echo "</table>";
?>
    <script>
        $(document).ready(function(){
            
            $('#add_proj_img').click(function(){
                var count = $('.proj_upload').length;
                $('#proj_images_cell').append(' <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">'+
                    '<input type="text" class="proj_upload" name="proj_img[]" id="proj_upload_'+count+'" value="" data-img-src="" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">'+
                '</div>');
                $('#proj_upload_'+count).imgupload2({
                    destroy_on_remove:true,
                    before_destroy : function () {
                        $('#proj_images_cell').append('<input type="hidden" name="delete_images[]" value="'+$('#proj_upload_'+count).val()+'">');
                    }
                });
            });
            
            // CKEDITOR.replace( 'editor1' );
            CKEDITOR.config.width = 600;
            CKEDITOR.config.width = '100%';
        });
    </script>
<?php
	echo "<tr><td colspan='2'>".form_submit('','Add Project')."</td></tr>";
	echo form_hidden($data_form4);
	echo form_close();
	echo "</table>";

	include('footer.php');
?>