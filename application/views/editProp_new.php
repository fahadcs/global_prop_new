<?php include('header.php'); ?>

<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Project</li>
              </ol>
              <h2>Edit Project</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->
    <section class="project-create">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 offset-lg-1">
              <div class="card shadow">
                  <form action="" method="POST">
                  <div class="form-group pt-0">
                    <label for="">Project Title</label>
                    <input type="text" class="form-control" placeholder="Test 1">
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label for="">Project Description</label>
                    <h6>CK Editor Widget Here</h6>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label for="">Project Value</label>
                    <input type="text" class="form-control" placeholder="$568200">
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label for="">Images</label>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <div class="uploaded-images">
                      <div class="row">
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <img class="img-fluid" src="img/property-images/1.jpg" alt="">
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>
                          </div>                          
                          <div class="col-md-3 col-6">
                            <div class="position-relative"> 
                              <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                        </div>
                    </div>
                    <div class="form-group border-bottom-0">
                    <label for="">Video</label>
                    <input type="text" class="form-control" placeholder="Youtube or Vimeo Links Past Here">
                    <p class="or">OR</p>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <div class="uploaded-images">
                      <div class="row">
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <video autoplay controls>
                                <source src="property.mp4" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Your browser does not support the video tag.
                              </video>
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <video autoplay controls>
                                <source src="property.mp4" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Your browser does not support the video tag.
                              </video>
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>
                          </div>                          
                          <div class="col-md-3 col-6">
                            <div class="position-relative"> 
                              <video autoplay controls>
                                <source src="property.mp4" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Your browser does not support the video tag.
                              </video>
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                          <div class="col-md-3 col-6">
                            <div class="position-relative">
                              <video autoplay controls>
                                <source src="property.mp4" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Your browser does not support the video tag.
                              </video>
                              <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                            </div>                            
                          </div>
                        </div>
                    </div>
                  </div><!-- /.form-group -->
                  <div class="add-update">
                      <div class="add-update-title">
                        <h2>Project Update</h2>
                      </div>
                      <div class="form-group pt-0">
                        <label for="">Project Title</label>
                        <input type="text" class="form-control">
                      </div><!-- /.form-group -->
                      <div class="form-group">
                        <label for="">Project Description</label>
                        <h6>CK Editor Widget Here</h6>
                      </div><!-- /.form-group -->
                      <div class="form-group">
                        <label for="">Project Value</label>
                        <input type="text" class="form-control">
                      </div><!-- /.form-group -->
                      <div class="form-group">
                          <label for="">Images</label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                          <div class="uploaded-images">
                            <div class="row">
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <img class="img-fluid" src="img/property-images/1.jpg" alt="">
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>
                                </div>                          
                                <div class="col-md-3 col-6">
                                  <div class="position-relative"> 
                                    <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <img class="img-fluid" src="img/property-images/2.jpg" alt="">
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                              </div>
                          </div>
                          <div class="form-group border-bottom-0">
                          <label for="">Video</label>
                          <input type="text" class="form-control" placeholder="Youtube or Vimeo Links Past Here">
                          <p class="or">OR</p>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                          <div class="uploaded-images">
                            <div class="row">
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <video autoplay controls>
                                      <source src="property.mp4" type="video/mp4">
                                      <source src="movie.ogg" type="video/ogg">
                                      Your browser does not support the video tag.
                                    </video>
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <video autoplay controls>
                                      <source src="property.mp4" type="video/mp4">
                                      <source src="movie.ogg" type="video/ogg">
                                      Your browser does not support the video tag.
                                    </video>
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>
                                </div>                          
                                <div class="col-md-3 col-6">
                                  <div class="position-relative"> 
                                    <video autoplay controls>
                                      <source src="property.mp4" type="video/mp4">
                                      <source src="movie.ogg" type="video/ogg">
                                      Your browser does not support the video tag.
                                    </video>
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                                <div class="col-md-3 col-6">
                                  <div class="position-relative">
                                    <video autoplay controls>
                                      <source src="property.mp4" type="video/mp4">
                                      <source src="movie.ogg" type="video/ogg">
                                      Your browser does not support the video tag.
                                    </video>
                                    <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                                  </div>                            
                                </div>
                              </div>
                          </div>
                        </div><!-- /.form-group -->
                      </div>
                  </div>
                  <button type="button" class="btn btn-primary mr-2">Confirm Changes</button>
                  <button type="button" class="btn btn-primary add-update-btn">Add Update</button>
                   <button type="button" class="btn btn-primary add-update-remove">Remove Update</button>
              </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.project-lists -->

<?php include('footer.php'); ?>