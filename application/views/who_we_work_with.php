<?php include('header.php') ?>

<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Who We Work With</li>
              </ol>
              <h2>Who We Work With</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->

    <section class="content-wrapper about-us">
        <div class="container">
          <div class="Who-We-Work-With">  
            <div class="row">
              <div class="col-lg-12">
                <img class="img-fluid" src="http://placehold.jp/1200x500.png" alt="">
                <br><br>
                <p>We work with builders primarily, because they are the first line of responsibility during the construction phase. An individual is quite welcome to contact us and seek funding of a construction project, but that project will not make it onto our listings if it doesn't meet our strict criteria. And the individual who initiates the project will be on the hook along with the builder, regaurdless of their credit worthiness, which is not of primary interest to GLOBALPROP, because we will sell the project to someone, even if the initiating party falls through. Some of the other criteria are as follows.</p>      
                <ol>
                  <li>Formost, we have to approve a local contractor with credentials and confirmable budget discipline.</li>
                  <li>Any GLOBALPROP contractor must agree to funds disbursment based only on pre-detemined completed stages and purchased materials. GLOBALPROP funds only go out on COMPLETED operations. Clearly, a contractor will have to purchase the materials themselves in order to complete a phase, so we never risk your investment in that way.</li>
                  <li>The construction cost and sale price have to be firmly fixed, realistic, and sufficiently profitable for the specific market.</li>
                  <li>Economic projections for the local market have to be stable to positive with no forseeable negative volitility in any metric.</li>
                  <li>Local law must provide for GLOBALPROP to initiate pre-emtive leans on all parties involved.</li>
                </ol>       
              </div>
            </div>  
          </div>          
        </div><!-- /.container -->
    </section><!-- /.project-lists -->




<?php include('footer.php') ?>