<?php include('header.php'); ?>
<div class="banner">
      <div class="container">
        <div class="row">
            <div class="col-lg-5">
               <h2>Online real estate investments</h2>
               <h3>Simple and quick</h3>
               <a href='<?php echo base_url()."users/regi"; ?>' class="btn btn-info">Invest Now</a>                
            </div>
            <div class="col-lg-7">
              <img class="img-fluid" src="imgs/banner.svg" alt="">
            </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
   </div><!-- /.banner --> 



  <section class="project-lists bg-light">
        <div class="container">
          <div class="row project-lists-header">
            <div class="col-lg-6">
              <h2>Current Projects</h2>
              <hr>
            </div>
            <?php if($usertype == 'admin' || $usertype == ' builder') { ?>
            <div class="col-lg-6">
              <div class="create-new-project">
                <a href="<?php echo base_url(); ?>props/newProp/" class="btn btn-primary">Create New Project</a>
              </div>
            </div>
            <?php } ?>
          </div>

          <div class="row">
            <?php 
              if(!isset($props)){
                  echo "There are currently no poperties to invest in.";
              }else{
                  foreach($props as $row){
                      if($row['active'] != 1 && $usertype != 'admin'){
                          continue;
                      }                    
              ?>
            <div class="col-lg-4 col-md-6">
            <a href="<?php echo base_url(); echo "props/singleProp/".$row['projID'];  ?>" class="" style="text-decoration:none">
                <div class="card h-100">
                  <div class="hover-change-image bg-hover-overlay rounded-lg card-img-top">
                    <!-- <img class="img-fluid" src="imgs/project-img/small/1.jpg" alt=""> -->
                    <img class="img-fluid" src="<?php if($row['front_image'] != ''){echo base_url(); echo $row['front_image']; } else { echo base_url()."uploads/2021/12/a946be1870d4bd4c7bc2f6d7d46008ef.jpg"; } ?>" alt="" style="width:347px; height:232px" >                   
                  </div>
                  <div class="card-header bg-transparent d-flex justify-content-between align-items-center py-2">
                    <p class="font-weight-bold text-heading mb-0 lh-1">
                      <span>Amt Funded</span><br>
                      <?php echo $row['amtFunded']; ?>
                    </p>   
                    <p class="fs-17 font-weight-bold text-heading lh-1">
                      <span>Amt Amt</span><br>
                      <?php echo $row['projAmt']; ?>
                    </p>                 
                  </div>
                  <div class="card-body py-2 pb-3" style="height: 200px;">
                    <h2 class="fs-16 lh-2 mb-0"><span class="text-dark hover-primary"><?php echo $row['title']; ?></span></h2>
                    <?php 
                    $string = $row['descr'];
                    if (strlen($string) > 240) {
                    
                        // truncate string
                        $stringCut = substr($string, 0, 240);
                        $endPoint = strrpos($stringCut, ' ');
                        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                        $string .= '......';
                    }
    
                      ?>
                    <p class="font-weight-500 text-gray-light mb-0"><?php echo $string; ?></p>
                 
                  </div>
                  

                          <?php if($usertype == 'admin'){ ?>
                            <div class="card-img-bottom card-img-"> 
                            <a href="<?php echo base_url(); ?>props/editProp/<?php echo $row['projID']; ?>" class="btn btn-primary" >Edit</a>
                            <a href="<?php echo base_url(); echo "props/deleteProp/".$row['projID'];  ?>" class="btn btn-primary">Delete</a> 
                            </div>        
                          <?php } ?>

                          <?php if($usertype == 'builder' && $userID == $row['userID']){ ?>
                            <div class="card-img-bottom card-img-"> 
                            <a href="<?php echo base_url(); ?>props/editProp/<?php echo $row['projID']; ?>" class="btn btn-primary" >Edit</a>
                            <a href="<?php echo base_url(); echo "props/deleteProp/".$row['projID'];  ?>"  class="btn btn-primary">Delete</a>
                            </div>
                           <?php } ?>
                           
                      
                         <?php  if($usertype == 'admin'){  ?>
                        <div class="card-img-bottom">
                          <?php if($row['active']==1){$switch= 'Disapprove';}else{$switch= 'Approve';} ?>
                          <a href="<?php echo base_url(); echo "props/approveProp/".$row["projID"]."/".$row['active'] ?>" class="btn btn-info btn-block"><?php echo $switch ?></a>
                        </div> 
                       <?php } ?> 
                     
                       
                                   
              </div>
            </a>
            </div><!-- /.col-lg-4 col-md-6 -->
            <?php 
                    } // end of foreach
        
                } // end of else 
            ?>



          </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.project-lists --> 
    <section class="how-it-work">
      <div class="container">
        <div class="row project-lists-header">
            <div class="col-lg-12">
              <h2>How does it work?</h2>
              <hr>
            </div>
        </div>
        <div class="feature-box">
          <div class="row">
            <div class="col-lg-6">
              <img class="img-fluid" src="http://via.placeholder.com/800x500" alt="">
            </div>
            <div class="col-lg-6">
              <div class="feature-box-l">
                <h2>Login and Registration</h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.</p>  
              </div>
            </div>
          </div>
        </div>
        <div class="feature-box">
          <div class="row">
            <div class="col-lg-6 order-lg-1">
              <img class="img-fluid" src="http://via.placeholder.com/800x500" alt="">
            </div>
            <div class="col-lg-6 order-lg-0">
              <div class="feature-box-r">
                <h2>Dashboard</h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.</p>  
              </div>
            </div>
          </div>
        </div>
        <div class="feature-box">
          <div class="row">
            <div class="col-lg-6">
              <img class="img-fluid" src="http://via.placeholder.com/800x500" alt="">
            </div>
            <div class="col-lg-6">
              <div class="feature-box-l">
                <h2>Login and Registration</h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.</p>  
              </div>
            </div>
          </div>
        </div>
        <div class="feature-box">
          <div class="row">
            <div class="col-lg-6 order-lg-1">
              <img class="img-fluid" src="http://via.placeholder.com/800x500" alt="">
            </div>
            <div class="col-lg-6 order-lg-0">
              <div class="feature-box-r">
                <h2>Dashboard</h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsum dicta nesciunt nemo reiciendis ipsam minus architecto nulla nisi, voluptatem, est. Tenetur, ab aliquid odit in.</p>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="cta">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2>Get access to secured property deals and start earning great returns on your investment.</h2>
            <a href='<?php echo base_url()."users/regi"; ?>' class="btn btn-dark">Register now</a>
          </div>
        </div>
      </div>
    </section>
    


<?php include('footer.php'); ?>