<?php include('header.php');
  echo "<h1>Register</h1>";
  if(!empty($errors)){
	  echo "<div style='background:red; color:white;'>".$errors."</div>";
  } 
  echo form_open(base_url().'users/regi');
  $data_form1=array(
  	'name'=>'username',
	'size'=>50,
	'style'=>'border:1px solid black',
	'id'=>'username'
  );
  $data_form2=array(
  	'name'=>'email',
	'size'=>50,
	'style'=>'border:1px solid black',
  );
  $data_form3=array(
  	'name'=>'password',
	'size'=>50,
	'style'=>'border:1px solid black',
	'id'=>'password'
  );
  $data_form4=array(
  	'name'=>'password2',
	'size'=>50,
	'style'=>'border:1px solid black',
	'id'=>'password2'
  );
  $data_form5=array(
  	''=>'---',
	'investor'=>'Investor',
	'builder'=>'Builder',
  );
  echo "<div style='width:370px;'><table class='table'>";
  echo "<tr><td>".form_label('Username','username')."</td><td>".form_input($data_form1)."</td></tr>";
  echo "<tr><td>".form_label('Email','email')."</td><td>".form_input($data_form2)."</td></tr>";
  echo "<tr><td>".form_label('Password','password')." </td><td>".form_password($data_form3)."</td></tr>";
  echo "<tr><td>".form_label('Password2','password2')." </td><td>".form_password($data_form4)."</td></tr>";
  echo "<tr><td colspan='2'>".form_dropdown('user_type',$data_form5, '')."</td></tr>";
  echo "<tr><td colspan='2'>".form_submit('','Register')."</td></tr>";
  echo "</table></div>";
  echo form_close();

	include('footer.php');
?>	