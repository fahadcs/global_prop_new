<?php include('header.php'); 
	echo "<h1>Edit Project Update</h1>";
    echo '<h4>for project</h4><h3>'.$prop['title'].'</h3>';
	$data_form_desc= array(
		'id'=>'descript',
		'name'=>'update_descr',
		'value'=>$proj_update['descr'],
		'rows'=>4,
        'class'=>'ckeditor'
	);
?>
    <script src="<?php echo base_url("assets/js/jquery.form.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload.js?v=5"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload2.js?5"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js?5"); ?>"></script>
    <form action="<?= base_url().'props/editPropUpdate/'.$proj_update['updateID']; ?>" method="post" accept-charset="utf-8">
<?php
	echo "<table class='table'>";
	echo '<tr><td style="width:17%;">'.form_label('Description','descript').' </td><td>'.form_textarea($data_form_desc).'</td></tr>';
?>
    <tr>
        <td>
            <label for="">Images</label>
        </td>
        <td id="updates_images_cell" style="max-width:200px;">
            <div style="margin-bottom:10px;"><button class="btn btn-default" id="add_update_img" type="button">Add Image</button></div>
            <?php
            $img_count = 0;
            foreach ( $proj_update_images as $i=>$image ) {
            ?>
                <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">
                    <input type="text" class="update_upload" name="update_img[]" id="update_upload_<?= $img_count++; ?>" value="<?= $image['imgID']; ?>" data-img-src="<?= base_url($image['full_path']); ?>" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">
                </div>
            <?php
            }
            ?>
        </td>
    </tr>
<?php
	echo "</table>";
?>
    
    <div class="row">
        <button class="span2" type="submit">Confirm Changes</button>
    </div>
    
    </form>
    <script>
        $(document).ready(function(){
            $('#add_update_img').click(function(){
                var count = $('.update_upload').length;
                $('#updates_images_cell').append(' <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">'+
                    '<input type="text" class="update_upload" name="update_img[]" id="update_upload_'+count+'" value="" data-img-src="" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">'+
                '</div>');
                $('#update_upload_'+count).imgupload2({
                    destroy_on_remove:true
                });
            });
            
            <?php
            $img_count = 0;
            foreach ( $proj_update_images as $i=>$image ) {
            ?>
                $('#update_upload_'+<?= $img_count; ?>).imgupload2({
                    destroy_on_remove:true,
                    before_destroy : function () {
                        $('#update_upload_'+<?= $img_count; ?>).closest('form').append('<input type="hidden" name="delete_images[]" value="'+$('#update_upload_'+<?= $img_count++; ?>).val()+'">');
                    }
                });
            <?php
            }
            ?>
        });
    </script>

<?php
	include('footer.php');
?>
    
    