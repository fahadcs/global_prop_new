<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Font Awesome icons (free version)-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <!-- Main CSS -->

    <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>">
    <title></title>
  </head>
  <body>
  <?php 
  $userID=""; $usertype=""; $username=""; $userRef='';
  if($this->session->userdata('userID')){
	  $roiPercent= 0;
	$userID= $this->session->userdata('userID');
	$usertype= $this->session->userdata('user_type');
	$username= $this->session->userdata('username');
	$userRef=  $this->session->userdata('referralID');
    $regi_login= "<li class='nav-item'> <a class='nav-link' href='".base_url()."users/account/"."'>My Profile</a> </li>
                    <li class='nav-item'> <a class='nav-link btn btn-primary' href='".base_url()."users/logout/'>Logout</a> </li>";
  }else{
    $regi_login= "<li class='nav-item'> <a class='nav-link' href='".base_url()."users/login'>Login</a> </li> <li class='nav-item'> <a class='nav-link btn btn-primary' href='".base_url()."users/regi'>Register</a> </li>";  
  }
?>

          

    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="" href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>/imgs/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?php echo base_url();?>">Home</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="<?php echo base_url(); ?>props/about" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                About us
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item"  href="<?php echo base_url();?>props/work">Who We Work With</a>   
                  <a class="dropdown-item" href="<?php echo base_url();?>props/referral">Our Referral Program</a>             
                  <a class="dropdown-item" href="<?php echo base_url();?>props/about">About</a>             
              </div>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>props/about" id="navbarDropdown" role="button">About</a>
            </li>  -->

            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>props">Projects</a>
            </li>   
            <?php echo $regi_login; ?>      
          </ul>          
        </div>
      </div><!-- /.container -->
    </nav>
 
    