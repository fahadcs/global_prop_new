<?php include('header.php') ?>


    
    <div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url();?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Register</li>
              </ol>
              <h2>Register</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->

    <div class="wrapper">
      <div class="container">
        <div class="row">
          <div class="col-md-6 offset-lg-3">
              <div class="card shadow border-0 login-card">
                  <h2>Register</h2>
                  <?php if(!empty($errors)){
                            echo "<div style='background:red; color:white;'>".$errors."</div>";
                        }  ?>
                  <div class="card-body">
                      <form class="needs-validation" novalidate action="<?php echo base_url();?>users/regi" method="POST">
                        <div class="form-group">
                          <label>Username</label>
                          <input type="text" class="form-control" name="username" placeholder="Enter Username" required>      
                          <div class="invalid-feedback">
                            Please provide a valid Username.
                          </div>                     
                        </div>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" class="form-control" name="email" placeholder="Enter Email" required>      
                          <div class="invalid-feedback">
                            Please provide a valid Email.
                          </div>                     
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" class="form-control" name="password" placeholder="Enter Password" required>
                          <div class="invalid-feedback">
                            Please provide a valid Password.
                          </div> 
                        </div>
                        <div class="form-group">
                          <label>Confirm Password</label>
                          <input type="password" class="form-control" name="password2" placeholder="Enter Password" required>
                          <div class="invalid-feedback">
                            Password is not Match.
                          </div> 
                        </div>

                        <div class="form-group">
                          <label>User Type</label>
                          <select class="form-control" id="user_type" name="user_type">
                            <option value="">----</option>
                            <option value="investor">Investor</option>
                            <option value="builder">Builder</option>
                        </select>
                          <div class="invalid-feedback">
                            Please Select User Type
                          </div> 
                        </div>

                        <button type="submit" class="btn btn-primary">Register</button>
                      </form>
                  </div>
              </div>
          </div><!-- /.col-lg-5 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.wrapper -->

    <?php include('footer.php') ?>