<?php

include('header.php');

?>
    <style>
        .img-port-link{
            color : #4ae;
            margin : 4px;
            border-radius: 6px;
            display : inline-block;
        }
        .img-port-link:hover{
            background-color : #4ae;
            text-decoration : underline;
        }
        .proj-img, .update-img{
            border-radius: 4px;
            margin : 3px;
        }
        .proj-img:hover, .update-img:hover{
            border-radius: 4px;
        }
    </style>
<?php
echo "<div id='container'>";

$msg2= "";  $msg3= "";
//we must check if the user is an investor and only add the invest table/form if it is
if($usertype== 'investor'){	
  if($proj['projAmt'] > $proj['amtFunded'] || $inv['confirm']== 0){
	    //Check confirm switch and if it is 1, set it back to 0
		//check if this is an edit of an existing pledge or a new pledge and change the messaging accordingly
		if($inv['amt']== 0){
			$form_data1= array(
					'name'=>'refNum',
					'placeholder'=>'referral code here',
			);
			$msg1= 'You have no investment yet.';
			$msg2= "<span style='font-size:.8em'>You can add your friend's referral number only the first time you 
			pledge on a project. The amount you pledge will go towards your friend's rewards total and work 
			to increase their ROI. <br/> <b><u>If you invite friends and they invest, your ROI can go up too.</u></b></style>";			
			$refCode= "<td>".form_label('Referral', 'referral').form_input($form_data1)."</td>";
		}else{
			
			if($refTot > 0){
			  $msg3= "Good News!!<br/><u>Someone Used Your Ref #</u><br/>Your Rewards: $".$refTot.".00";
			}else{
				$msg3= "Sorry...<br/><u>No one has Used Your Ref # yet</u><br/>Your Rewards: $".$refTot.".00";
			}//end if $refTot > 0
			  $msg1= 'Your amount in USD: $'.$inv['amt'].'.00';	
			  $msg2= "<span style='font-size:.8em'>You have already invested in this project, but you can increase your overall
					  RIO if you invite your friends to invest here. <br/>
					  <b><u>Make more when your friends invest in a project that you've funded.</u></b></style>";
			 //Check if the total amount funded is greater than the total amount needed and add confirm button
			 if($proj['projAmt'] <= $proj['amtFunded']){
				 if($inv['confirm']== 0){
				   $msg2= "<div style='background:#FFAEB5; width:150px;'><a href='".base_url().
				   "invests/confirm_pledges/".$proj['projID']."'>&nbsp;<b>Confirm Your Pledge</b></div></a><br/>
				   Please confirm your pledge so that the project can start. 
				   The Amount Pledged has exceeded the Amount Needed.<br/>";
				 }else{
					 $msg2= "<div style='background:#90FFA7;'><center>Congratulations! Your pledge is confirmed.</center>.</div>";
				 }//end if $inv['confirm']== 0
			 }else{
			 }//end if $proj['projAmt'] <= $proj['amtFunded']			  
			  $refCode= "<td><center>".$msg3."</center></td>".form_hidden('refNum',0);		
		}//end if $inv[amt]== 0
		
		
		
		$form_data2= array(
			'name'=>'invest',
			'placeholder'=>$inv['amt'],
		);
		
		//if the user is an investor $td2 var outputs the invest table/form 
		$td2= "<table>".form_open(base_url().'invests/new_invest')."
			  <tr>".$refCode.		  
				"<td>Your Referral #<br/> &nbsp; &nbsp; ** ".
				$userRef." **<br/>&nbsp; Invite a Friend
			  </td>
			  </tr>
			  <tr><td>".
				form_input($form_data2). form_hidden('projID',$proj['projID']). form_hidden('amtFunded',$proj['amtFunded']).
				"<br/><div style='background:green; color:white; width:220px;'>
					<center>".$msg1."</center>
				 </div></td><td>".form_submit('','Change Amount').
			   "</td</tr></table>"
			  .form_close();	
	  }else{
		  $td2= "<div style='background:red; color:white; width:220px;'>Investment is Currently Closed</div><br/>
		  		 <div style='background:green; color:white; width:220px;'>
					<center>Your amount in USD: $".$inv['amt'].".00</center>
				 </div>";
	  }//end if projAmt is greater than amtFunded
}else{
	//if the user is not an investor $td2 outputs a message instead of the invest table/form
	$td2= "<div style='background:orange; color:white; width:300px;'>
			<center>You must be logged in with an investor's account in order to invest in any projects.</center>
		  </div>";
}//end if $usertype== investor

echo "<table class='table'><tr>
		<td><h1>Project Details</h1>".$msg2."</td>
		<td>".$td2."</td>
	  </tr></table>";

if(!isset($proj)){
	echo "<p>This page was accessed incorrectly</p>";
}else{
	echo "<hr>
		<h3>".$proj['title']."</h3>".
		"<div style='background:blue; color:white;'><center>
		  Project Posted: ".$proj['date_added']." -- Project Total: $".$proj['projAmt'].".00 -- 
		  Amount Funded so-far: $".$proj['amtFunded'].".00</center>
		 </div>
		 <hr>" .$proj['descr'];
    
    if ( !empty($proj_images) ) {
        echo '<div>';
        foreach ( $proj_images as $proj_img ) {
            echo '<a class="img-port-link" href="'.base_url($proj_img['full_path']).'" data-lightbox="project-'.$proj['projID'].'">';
            echo '<img class="proj-img" src="'.str_replace($proj_img['raw_name'],$proj_img['raw_name'].'_120',base_url($proj_img['full_path'])).'">';
            echo '</a>';
        }
        echo '</div>';
    }
    
    echo "<hr>";
         
    if ( !empty($updates) ) {
        echo '<h4>Project Updates</h4>';
        echo '<hr style="margin-top:0px;">';
        foreach ( $updates as $upd ) {
            echo '<span class="pull-right">'.date('F d, Y', strtotime($upd['date_added'])).' - '.date('H:i', strtotime($upd['date_added'])).'</span><br>';
            if ( $usertype == 'admin' || ( $usertype == 'builder' && $this->session->userdata('userID') == $proj['userID'] ) ) {
                echo '<span class="pull-right"><div class="row">';
                echo '<a class="btn btn-info btn-small" style="color:white;" href="'.base_url('props/editPropUpdate/'.$upd['updateID']).'">Edit</a>';
                echo '<a class="btn btn-danger btn-small" style="color:white;margin-left:5px;" href="'.base_url('props/deletePropUpdate/'.$upd['updateID']).'">Delete</a>';
                echo '</div></span>';
            }
            
            echo $upd['descr'];
            if ( !empty($upd['upd_images']) ) {
                echo '<div>';
                foreach ( $upd['upd_images'] as $upd_image ) {
                    echo '<a class="img-port-link" href="'.base_url($upd_image['full_path']).'" data-lightbox="updates-'.$upd['updateID'].'">';
                    echo '<img class="update-img" src="'.str_replace($upd_image['raw_name'],$upd_image['raw_name'].'_120',base_url($upd_image['full_path'])).'">';
                    echo '</a>';
                }
                echo '</div>';
            }
            echo '<hr>';
        }
    }
		 
	
	if(isset($invList)){
	  echo "<h4>List of Current Investors</h4>";
	  echo "<table class='table'> <tr><th>Name</th> <th>Email</th> <th>Pledge</th></tr>";	  
	  foreach($invList as $row ){
		  echo "<tr><td>".$row['username']."</td><td>".$row['email']."</td><td>$".$row['amt'].".00</td></tr>";
	  }
	  echo "</table>";
	}
}
?>
    
    <script src="<?php echo base_url("assets/js/lightbox.js"); ?>"></script>
    <script>
        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
        })
    </script>
<?php

include('footer.php');
