<?php include('header.php') ?>

<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">About us</li>
              </ol>
              <h2>About us</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->

    <section class="content-wrapper about-us">
        <div class="container">
          <div class="row project-lists-header">
            <div class="col-lg-12">
              <h2>How GLOBALPROP Investment Works</h2>
              <hr>
            </div>
          </div>
          <div class="investor-work">            
            <div class="row">
              <div class="col-lg-12">
                <h3>ROI From 8% To 12%</h3>
              </div>
            </div>    
            <div class="row">
              <div class="col-lg-12">
                <p>P2P investing is a rising new means to invest personal funds with more control. It is hands on for the investor, and hands off for the the Western Banking System, which has traditional lending cornered in a way that redirects earning potential more to to them and less to you. But most P2P sites place your investments in risky consumer debt consolodation, or sketchy small business loans. Not surprisingly, high rates of default eat deeply into the advertized returns. There is little in the way of recourse should your money disappear.</p>

                <p>Furthermore, most P2P lending options still rely on traditional banking for transactions, which limits their activity to the national venue of origination. In other words, it's hard to invest internationally.</p>

                <p>Investing money in new construction substantially reduces the risks and gives individuals a much safer investment option. If projects are carefully selected and managed correctly, new construction can be quickly sold as an exit strategy, and overall project returns of 30% can be dependably achieved time after time. This creates enough profit room to motivate both builders and investors.</p>

                <p>For those who wish to invest internationally in new construction projects, bitcion solves the funds transfer issues. It costs only 1% to convert currency into bitcoin, and going from digital currency back to analog is free. Together, using new construction and bitcoin, we can create an exciting new category of both domestic and international real estate investment.</p>
              </div>
            </div>  
          </div>
          <div class="row phase">
              <div class="col-lg-12">
                <h3><span>Phase 1</span>: The Pledge Phase</h3>
                <p>Working with GLOBALPROP is easy. It starts by selecting a proposed project and pledging funds. A pledge is just a tentative promise, however, and no funds actually change hands during this phase. You may choose to pledge as little as a dollar, or as much as $100,000.00. It all depends on you. At any investment level, you will recieve between 8% and 13% per year on anything you have invested into any completed project.</p>
                <p>The project will not commence, however, during the Pledge Phase. As pledges increase, they will get closer and closer to the project's target funds amount, and the Pledge Phase will not end until we have collected enough pledges to meet that monetary goal.</p>
                <p>Once pledges do reach the project's target, it will be time to confirm your pledge, and everyone else will confirm at the same time. You may also wish to change your pledge amount, increasing or decreasing it, or even withdrawing your pledge all together. In fact you can perform these options at any time, since during the Pledge Phase your account is always open to you and editable at all times before confirmation occurs. But what happens if you confirm your pledged amount, but other investors decide to reduce or withdraw their pledges?</p>
                <p>In the event that previously pledged funds levels fall below the project's target amount once again during the confimation period, all confimations will be reversed, and the project will remain in the Pledge Phase. So as before, you will be free to change or withdraw your pledge until such time that the total amount pledged reaches the projects's target amount once again. And this process repeats until enough pledges are confirmed so as to truely meet or exceed the project's target funds amount. That is when you pledge can no longer be changed.</p>
              </div>
            </div>  
            <div class="row phase">
              <div class="col-lg-12">
                <h3><span>Phase 2</span>: The Funding Phase</h3>
                <p>Funding is simple. If your investment is deposited with us in the currency that is local to the project, you simply transfer the funds. On the other hand, if your deposit will need to be converted for use in the target project's currency environment, we simply use bitcoin. This avoids the costly fees involved in international funds transfer and currency exchange. You can get bitcoin at any of the banks on this list (places to buy bitcoin).</p>
                <p>Of course, your funds will be tied up from the time of deposit until the investment term matures. The good news is that few construction projects will exceed 12 months duration, and the estimated time of completion for any project is disclosed in each project's respective investment profile, so you will know exactly what to expect in that regard.</p>
              </div>
            </div>  
            <div class="row phase">
              <div class="col-lg-12">
                <h3><span>Phase 3</span>: The Operations Phase</h3>
                <p>The construction phase is exciting for GLOALPROP investors, because you will actually SEE you money being grown. Day by day, week by week, improvements will be made to the property of which you are part owner, and as it's value increases, you can viscerally see your investment grow too. Most projects will have streaming webcam set up, so you can check in and see the progress realtime if you desire to do so. Otherwise, for projects with or without the streaming footage, we will update the project with news and pictures regualrly and frequently.</p>
                <p>You will also be glad to know that there is a discussion board on each project page where investors, project managers, and GLOBALPROP administrators can answer questions or concerns all along in the process.</p>
              </div>
            </div>  
            <div class="row phase">
              <div class="col-lg-12">
                <h3><span>Phase 4</span>: The Exit Phase</h3>
                <p>Currently, all of our investment opportunities exit through the sale of newly constructed occupied spaces, and the effort to market and sell the property does not need to wait until the construction is complete. Whenever a buyer puts earnest money down on a property, it will be a good time to pop a cork and celebrate, because that means our ROI is now firmly in the bag. This entire process, from marketing until sale, is part of the body of information included in the project's regular updates.</p>
                <p>We expect that some investors will be interested in owning a portfolio of managed properties at some point, and so we at some point will begin to build up an inventory of 'P2P OWNED' land that we keep and manage. Some properties are more profitable than others, and as we come across or construct these types of "over achieving" sites, we will explore the interest of our investors in retaining ownership and creating more sophisticated investment opportunities, which will in turn require more sophisticated exit strategies. For now though, it is all pretty cut and dry. Sell it and pay out.</p>
              </div>
            </div> 
            <div class="row phase">
              <div class="col-lg-12">
                <h3><span>Phase 5</span>: The Pay-Out Phase</h3>
                <p>This phase is quite simple, except that there are some particulars regarding exactly how much you will recieve. First of all, there will be a projected time of completion for all projects, and this will be posted as a commitment to you, on any project investment profile during the Pledge Phase. If any adjustments need to be made to that schedule projection, it will be done prior to the confirmation of your pledge so that you can re-evaluate your interest on that metric prior to pulling the trigger and actually confirming your pledge.</p>
                <p>Following that, there are three potential outcomes schedule-wise, and they are as follows.</p>
                <ol>
                  <li>Schedule is met - Your ROI is as expected.</li>
                  <li>Completion is early - Your ROI is as expected. We will not reduce your ROI if the term is shorter than expected!</li>
                  <li>Project runs over intended schedule - Your interest continues to increase at the same rate until completion.</li>
                </ol>
                <p>The best outcome, of course, is when a project is completed and sold ahead of schedule. In these cases, you recieve your full expected percentage based on the origonal schedule, but you get that back early, effectively increasing your ROI overall.</p>
              </div>
            </div><!-- /..phase -->
            <div class="row">
              <div class="project-lists-header mb-0">
                <div class="col-lg-12">
                  <h2>Different Investment Types</h2>
                  <hr>
                </div>
              </div>
              <div class="investor-work">
                <div class="col-lg-12">
                  <p>We still rate our investments on a scale of A through C, even though that is not based on risk factors related to any of our projects. None of our projects proceed on any grounds other than A rated opportunity. However, we do rate the risk on the basis of amount invested, because a high dollar investor is risking more capital, while and a low dollar investor is risking a lower position in the event a project does go south for some reason. Following is a run down on the ratings and the variable ROI associated with each.</p>                  
                </div> 
              </div>              
            </div>  
            <div class="row">
              <div class="col-lg-8 offset-lg-2">
                  <ul class="list-unstyled">
                    <li class="media">
                      <!-- <img src="img/a.png" class="mr-3" alt="..."> -->
                      <img src="<?php echo base_url();?>/imgs/a.png"  class="mr-3" alt="..." ><br/>
                      <div class="media-body">
                        <p>An A rated pledge has FIRST position and represents any investment equal to or over $10,000.00 USD.</p>
                        <p>The value of an A rated inevstment is 8% ROI.</p>
                      </div>
                    </li>
                    <li class="media my-4">
                      <!-- <img src="img/b.png" class="mr-3" alt="..."> -->
                      <img src="<?php echo base_url();?>/imgs/b.png" class="mr-3" alt="..." ><br/>
                      <div class="media-body">
                        <p>A B rated pledge has SECOND position and represents any investment equal to or over $2,500.00 but under $10,000.00 USD.</p>
                        <p>The value of an B rated inevstment is 9% ROI.</p>
                      </div>
                    </li>
                    <li class="media">
                      <!-- <img src="img/c.png" class="mr-3" alt="..."> -->
                      <img src="<?php echo base_url();?>/imgs/c.png" class="mr-3" alt="..."><br/>
                      <div class="media-body">
                        <p>A C rated pledge has THIRD position and represents any investment equal to or over $500.00 but under $2,500.00 USD.</p>
                        <p>The value of an C rated inevstment is 10% ROI.</p>
                      </div>
                    </li>
                    <li class="media">
                      <!-- <img src="img/d.png" class="mr-3" alt="..."> -->
                      <img src="<?php echo base_url();?>/imgs/d.png" class="mr-3" alt="..."><br/>
                      <div class="media-body">
                        <p>A D rated pledge has FORTH position and represents any investment under $500.00 USD.</p>
                        <p>The value of an D rated inevstment is 11% ROI.</p>
                      </div>
                    </li>                    
                  </ul>
              </div>
            </div>
            <div class="row investor-work pb-0">
              <div class="col-lg-12">
                <p>As with any investment, it is important to realize that there is a level of risk involved. Real Estate is like the stock market in that the markets do change, and market forces can impact real estate prices, as well as the mood of buyers. The benefit of real estate over the stock market is that volatility is historically much lower and slower overall. This having been said, remember that all investments have some risk, and smart investors account for that in their strategies.</p>
              </div>
            </div>   
        </div><!-- /.container -->
    </section><!-- /.project-lists -->

<?php include('footer.php') ?>