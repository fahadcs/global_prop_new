<?php include('header.php'); ?>

<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url();?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
              </ol>
              <h2>Login</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
</div><!-- /.page-header -->


<div class="wrapper">
      <div class="container">
        <div class="row">
          <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
              <div class="card shadow border-0 login-card">
                  <h2>Login</h2>
                  <div class="card-body">
                      <form  class="needs-validation" novalidate action="<?php echo base_url();?>users/login" method="POST">
                        <div class="form-group">
                          <label>Username</label>
                          <input type="text" class="form-control" placeholder="Enter Username" name="username" required>   
                          <div class="invalid-feedback">
                            Please provide a valid Email.
                          </div>                        
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
                          <div class="invalid-feedback">
                            Please provide a valid Password.
                          </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                      </form>
                  </div>
              </div>
          </div><!-- /.col-lg-5 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
</div><!-- /.wrapper -->

<?php include('footer.php'); ?>