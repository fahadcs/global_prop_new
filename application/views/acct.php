<?php include('header.php'); ?>
<div class="page-header bg-light">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <ol class="breadcrumb bg-transparent pl-0 mb-0">
                <li class="breadcrumb-item"><a class="text-primary" href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Your Account Manager</li>
              </ol>
              <h2>Your Account Manager</h2>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-header -->


    <section class="project-create admin-acount">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 offset-lg-1">
              <div class="card shadow">
                <div class="card-body p-0 border-0">
                  <h2 class="text-center">Your Account Manager</h2>   
                  <h3>Your Account Manager</h3>
                  <form action="<?php echo base_url(); ?>users/update_user" method="POST">
                  <div class="form-group pt-0 border-0">
                    <label for="">Change Username</label>
                    <input type="text" name="username" class="form-control" placeholder="administration" value="<?php echo $username; ?>">
                  </div><!-- /.form-group -->
                  <div class="form-group pt-0 border-0">
                    <label for="">Change Email</label>
                    <input type="email" name="email" class="form-control" placeholder="dragnetmarketing1@gmail.com" value="<?php echo $this->session->userdata('email'); ?>">
                  </div><!-- /.form-group -->
                  <div class="form-group pt-0 border-0">
                    <label for="">Change Password</label>
                    <input type="password" name="password" class="form-control" placeholder="">
                  </div><!-- /.form-group -->
                  <button type="submit" class="btn btn-primary d-inline-block">Change Credentials</button>
                  <hr>
                  <?php if($usertype== 'admin'){ ?>
                  <h3>Builder Listing</h3>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php 
                                foreach($bldrs as $row){
                          ?>
                        <tr>                       
                          <td><a href="<?php echo base_url(); ?>users/superLogin/<?php echo $row['userID']; ?>"><?php echo $row['username']; ?></a></td>
                          <td><?php echo $row['email']; ?></td>                        
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <hr>
                  <h3>Investor Listing</h3>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                                foreach($invrs as $row){
                          ?>
                        <tr>                       
                          <td><a href="<?php echo base_url(); ?>users/superLogin/<?php echo $row['userID']; ?>"><?php echo $row['username']; ?></a></td>
                          <td><?php echo $row['email']; ?></td>                        
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } ?>

                  <?php if($usertype== 'builder'){ ?>
                  <h3>All Projects</h3>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Title</th>
                          <th scope="col">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php 
                                foreach($projects as $row){
                                    if($row['active']== 0){$stat= 'Not Approved';}else{$stat= 'Approved';}
                          ?>
                        <tr>                       
                          <td><a><?php echo $row['title']; ?></a></td>
                          <td><?php echo $stat; ?></td>                        
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
                  <?php } ?>

                  <?php if($usertype== 'investor'){ ?>
                  <h3>All Investments</h3>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Titles</th>
                          <th scope="col">Amounts</th>
                          <th scope="col">Intrest</th>
                          <th scope="col">ROI</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php 
                                foreach($pledges as $row){
                                    if($row['active']== 0){$stat= 'Not Approved';}else{$stat= 'Approved';}
                          ?>
                        <tr>                       
                          <td><a href="<?php echo base_url(); ?>props/singleProp/<?php echo $row['projID']; ?>"><?php echo $row['title']; ?></a></td>
                          <td>$<?php echo $row['amt']; ?>.00</td> 
                          <td>$<?php echo $row['intrest']; ?>.00</td>
                          <td>$<?php echo ceil($row['amt'] * $row['intrest']); ?>.00</td>                       
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.project-lists -->

    <?php include('footer.php'); ?>