<?php include('header.php'); 
	echo "<h1>Edit Projects</h1>";
	$data_form1= array(
		'id'=>'title',
		'name'=>'tit',
		'value'=>$prop['title'],
	);
	$data_form2= array(
		'id'=>'descript',
		'name'=>'desc',
		'value'=>$prop['descr'],
		'rows'=>4,
        'class'=>'ckeditor'
	);
	$data_form3= array(
		'id'=>'p_amt',
		'name'=>'proj_amt',
		'value'=>$prop['projAmt'],
	);
	
?>
    <script src="<?php echo base_url("assets/js/jquery.form.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload.js?v=5"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.imgupload2.js?5"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js?5"); ?>"></script>
    <form action="<?= base_url().'props/editProp/'.$prop['projID']; ?>" method="post" accept-charset="utf-8">
<?php
	echo "<table class='table'>";
	echo "<tr><td>".form_label('Project Title','title')."</td><td>".form_input($data_form1)."</td></tr>";
	echo '<tr><td style="width:17%;">'.form_label('Description','descript').' </td><td>'.form_textarea($data_form2).'</td></tr>';
	echo "<tr><td>".form_label('Project Amount','p_amt')."</td><td>".form_input($data_form3)."</td></tr>";
?>
    <tr>
        <td>
            <label for="">Images</label>
        </td>
        <td id="proj_images_cell" style="max-width:200px;">
            <div style="margin-bottom:10px;"><button class="btn btn-default" id="add_proj_img" type="button">Add Image</button></div>
            <?php
            $img_count = 0;
            foreach ( $prop_images as $i=>$image ) {
            ?>
                <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">
                    <input type="text" class="proj_upload" name="proj_img[]" id="proj_upload_<?= $img_count++; ?>" value="<?= $image['imgID']; ?>" data-img-src="<?= base_url($image['full_path']); ?>" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">
                </div>
            <?php
            }
            ?>
        </td>
    </tr>
<?php
	echo "</table>";
?>
    <div id="updates-form" style="display:none;">
        <h2>Project Update</h2>
        <table class='table'>
            <tr>
                <td>
                    <label for="update_descr">Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                </td>
                <td>
                    <textarea id="update_descr" class="ckeditor" name="update_descr" rows="4" disabled="disabled"></textarea>
                </td>
            </tr>
            
            <tr>
                <td style="width:17%;">
                    <label for="">Images</label>
                </td>
                <td id="updates_images_cell" style="max-width:200px;">
                    <div style="margin-bottom:10px;"><button class="btn btn-default" id="add_update_img" type="button">Add Image</button></div>
                </td>
            </tr>
        </table>
    </div>
    
    <div class="row">
        <button class="span2" type="submit">Confirm Changes</button>
        <button id="update-form-button" class="span2 offset3" type="button">Add Update</button>
    </div>
    
    </form>
    <script>
        $.fn.toggleText = function(t1, t2){
            if (this.text() == t1) 
                this.text(t2);
            else
                this.text(t1);
            return this;
        };
        $.fn.toggleDisable = function(){
            if (this.is(':disabled')) {
                this.attr('disabled',false);
                CKEDITOR.instances.update_descr.setReadOnly(false);
                $('.update_upload').attr('disabled',false);
            }
            else {
                this.attr('disabled','disabled');
                CKEDITOR.instances.update_descr.setReadOnly(true);
                $('.update_upload').attr('disabled','disabled');
            }
            return this;
        };
        $(document).ready(function(){
            $('#update-form-button').click(function(){
                $('#update_descr').toggleDisable();
                $('#updates-form').slideToggle();
                $(this).toggleText('Remove Update', 'Add Update');
            });
            
            $('#add_proj_img').click(function(){
                var count = $('.proj_upload').length;
                $('#proj_images_cell').append(' <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">'+
                    '<input type="text" class="proj_upload" name="proj_img[]" id="proj_upload_'+count+'" value="" data-img-src="" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">'+
                '</div>');
                $('#proj_upload_'+count).imgupload2({destroy_on_remove:true});
            });
            
            $('#add_update_img').click(function(){
                var count = $('.update_upload').length;
                $('#updates_images_cell').append(' <div class="uploader-div" style="max-width:130px;color:white;vertical-align:top;margin-bottom:7px;margin-right:7px;">'+
                    '<input type="text" class="update_upload" name="update_img[]" id="update_upload_'+count+'" value="" data-img-src="" data-img-empty="<?= base_url("assets/img/empty_image.png"); ?>" data-upload-url="<?= base_url("props/upload_image"); ?>">'+
                '</div>');
                $('#update_upload_'+count).imgupload2({
                    destroy_on_remove:true,
                    before_destroy : function () {
                        $('#update_upload_'+count).closest('form').append('<input type="hidden" name="delete_images[]" value="'+$('#proj_upload_'+count).val()+'">');
                    }
                });
            });
            
            <?php
            $img_count = 0;
            foreach ( $prop_images as $i=>$image ) {
            ?>
                $('#proj_upload_'+<?= $img_count; ?>).imgupload2({
                    destroy_on_remove:true,
                    before_destroy : function () {
                        $('#proj_upload_'+<?= $img_count; ?>).closest('form').append('<input type="hidden" name="delete_images[]" value="'+$('#proj_upload_'+<?= $img_count++; ?>).val()+'">');
                    }
                });
            <?php
            }
            ?>
        });
    </script>

<?php
	include('footer.php');
?>