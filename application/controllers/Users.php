<?php

class Users Extends CI_Controller{
	function __construct() {
        parent::__construct();
		$this->load->model('user');
		$this->load->helper('form');
	}
	
	function login(){
		$data['error']= 0;
		if($_POST){
			$username= $_POST['username'];
			$password= $_POST['password'];
		
			$user= $this->user->login($username, $password);
			
			if(!$user){
				$data['error']= 1;
			}else{
				$this->session->set_userdata('userID', $user['userID']);
				$this->session->set_userdata('username', $user['username']);
				$this->session->set_userdata('email', $user['email']);
				$this->session->set_userdata('user_type', $user['user_type']);
				$this->session->set_userdata('referralID', $user['referralID']);
				redirect(base_url().'props');
			}
		}
		$this->load->view('login', $data);
	}
	
	function superLogin($userID){		
		$user= $this->user->super_login($userID);		
		$this->session->set_userdata('userID', $user['userID']);
		$this->session->set_userdata('username', $user['username']);
		$this->session->set_userdata('email', $user['email']);
		$this->session->set_userdata('user_type', $user['user_type']);
		$this->session->set_userdata('referralID', $user['referralID']);
		echo $this->session->userdata('username');
		redirect(base_url().'props');		
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'props');
	}
	
	function regi(){	
		
		$data['errors']=0;	
		if($_POST){
		  $config=array(
			array(
			  'field'=>'username',
			  'label'=>'Username',
			  'rules'=>'trim|required|min_length[3]|is_unique[users.username]',
			  'value'=>set_value('username'),
			),
			array(
			  'field'=>'email',
			  'label'=>'Email',
			  'rules'=>'trim|required|is_unique[users.email]|valid_email',
			  'value'=>set_value('email'),
			),
			array(
			  'field'=>'password',
			  'label'=>'Password',
			  'rules'=>'trim|required|min_length[5]',
			  
			),
			array(
			  'field'=>'password2',
			  'label'=>'Password2',
			  'rules'=>'trim|required|min_length[5]|matches[password]',
			),
			array(
			  'field'=>'user_type',
			  'label'=>'User Type',
			  'rules'=>'required',
			)			
		  );
		  $this->load->library('form_validation');
		  $this->form_validation->set_rules($config);
		  if($this->form_validation->run()== FALSE){
			  $data['errors']= validation_errors();
		  }else{
			$data=array(
				'username'=>$_POST['username'],
				'password'=>sha1($_POST['password']),
				'email'=>$_POST['email'],
				'user_type'=>$_POST['user_type'],
			);
			$userid=$this->user->create_user($data);
			$this->session->set_userdata('userID',$userid);
			$this->session->set_userdata('username', $_POST['username']);
			$this->session->set_userdata('user_type', $_POST['user_type']);
			if($_POST['user_type']== 'investor'){
			  $refNum= $this->session->userdata('userID')+13366;
			  $refData= array('referralID'=>$refNum);
			  $this->user->update_user($refData);
			}
			redirect(base_url().'props');				
		  }		  	  
		}		
		$this->load->view('regi.php', $data);
	}
	//the account function is run from acct.php view, and only handles the requests of primary users. 
	//Admin secondary requests are not run from this funtion
	function account(){
		$data= array();
		if($this->session->userdata('user_type')=='admin'){
			$data['bldrs']= $this->user->get_allUsers('builder');
			$data['invrs']=  $this->user->get_allUsers('investor');
		}
		if($this->session->userdata('user_type')=='builder'){
			$data['projects']= $this->user->get_bldrProjects($this->session->userdata('userID'));
		}
		if($this->session->userdata('user_type')=='investor'){			
			$data['pledges']= $this->user->get_pledges($this->session->userdata('userID'));
		}
		$this->load->view('acct', $data);
	}
	
	function update_user(){
		if($_POST){
			$data=array();
			if(!empty($_POST['username'])){$data['username']= $_POST['username'];}
			if(!empty($_POST['email'])){ $data['email']= $_POST['email'];}
			if(!empty($_POST['password'])){ $data['password']= sha1($_POST['password']);}
			$this->user->update_user($data);
			$this->session->sess_destroy();
			redirect(base_url().'props');
		}
	}
}

?>