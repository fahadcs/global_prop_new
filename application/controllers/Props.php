<?php

class Props extends CI_Controller{
	
	function __construct() {
        parent::__construct();
		$this->load->model('prop');
		$this->load->helper('form');
	}

    function faq(){
        $this->load->view("faq");	
    }
    function privacy_policy(){
        $this->load->view("privacy_policy");	
    }
    function term_of_use(){
        $this->load->view("term_of_use");	
    }
	
	function index($start= 0){
		$data['props']= $this->prop->get_props();		
		/*
		$this->load->library('pagination');
		$config['base_url']= base_url()."props/index";
		$config['total_rows']=$this->prop->get_props_count();
		$config['per_page']=5;
		$this->pagination->initialize($config);
		$data['pages']= $this->pagination->create_links();
		*/
        $result_array = [];

         foreach($data['props'] as $key => $value){
            $front_image = $this->prop->get_prop_front_image($value['projID']);
            if($front_image){
                $data['props'][$key]['front_image'] = $front_image[0]['full_path'];
            }
            else{
                $data['props'][$key]['front_image'] = '';
            }
            
        } 
		$this->load->view("prop_index", $data);		
	}
	
	/*singleProp function calls: 
		-> get_oneProp function from prop model  
		-> get_referrals function from invest model 
		-> get_allInvest function from the invest model
		-> get_singleInvst function from the invest model
	to the oneProp.php view*/
	function singleProp($projID){		
		$data['proj'] = $this->prop->get_oneProp($projID);
		$data['proj_images'] = $this->prop->get_prop_images($data['proj']['images']);
		$data['updates'] = $this->prop->get_props_update($projID, 3);

        foreach ($data['updates'] as $k=>$update) {
            $data['updates'][$k]['upd_images'] = $this->prop->get_prop_images($update['images']);
        }
        
        // var_dump($data['updates']);
        
		$this->load->model('invest');
        
		//$invList handles all the investors involved in this project
		$data['invList']= $this->invest->get_allInvest($projID);
		
		if($this->session->userdata('user_type')== 'investor'){	
            //$inv handles session user's pledges	  
            $data['inv']= $this->invest->get_singleInvst($projID); 

            //$refTot handles the total rewards the session user has for this project
            $referID= $this->session->userdata('referralID');

            $data['refTot']= $this->invest->get_referrals($projID, $referID, $data['inv']);
		}
		$this->load->view('oneProp', $data);
	}
	
	function newProp(){
		if($_POST){
			$data=array(
				'userID'=>$_POST['userID'],
				'title'=>$_POST['tit'],
				'descr'=>$_POST['desc'],
				'projAmt'=>$_POST['val'],
				'active'=>0,  
                'images'=>implode(',',$_POST['proj_img'])
			);
            
            if ( !empty($_POST['delete_images']) ) {
                // $this->prop->delete_proj_images( $_POST['delete_images'] );
            }
            
			
			$this->prop->insert_props($data);
            $this->prop->delete_unused_proj_images();
			redirect(base_url().'props/');
		}else{
			$this->load->view('newProp');
		}
	}
	
	function editProp($propID){
		$successUpdate= 0;
        
		if($_POST){
            if ( !empty($_POST['update_descr']) ) {
                $new_updates = array();
                $new_updates['descr'] = $_POST['update_descr'];
                $new_updates['projID'] = $propID;
                $new_updates['userID'] = $this->session->userdata('userID');
                if ( !empty( $_POST['update_img'] ) ) $new_updates['images'] = implode(',',$_POST['update_img']);
                $this->prop->insert_prop_update( $new_updates );
            } else
            if ( !empty( $_POST['update_img'] ) ) {
                $new_updates = array();
                $new_updates['projID'] = $propID;
                $new_updates['userID'] = $this->session->userdata('userID');
                $new_updates['images'] = implode(',',$_POST['update_img']);
                $this->prop->insert_prop_update( $new_updates );
            }
            
            if ( !empty($_POST['delete_images']) ) {
                // $this->prop->delete_proj_images( $_POST['delete_images'] );
            }
            
            
            // edits
			$data_update=array(
				'title'=>$_POST['tit'],
				'descr'=>$_POST['desc'],
				'projAmt'=>$_POST['proj_amt'],
                'images'=>implode(',',$_POST['proj_img'])
			);
			$this->prop->update_props($propID, $data_update);
            $this->prop->delete_unused_proj_images();
			$successUpdate= 1;			
		}
        
		$data['prop'] = $this->prop->get_oneProp($propID);
		$data['prop_images'] = $this->prop->get_prop_images($data['prop']['images']);
		$this->load->view('editProp', $data);
	}
    
    function editPropUpdate ( $updID ) {
        $usertype = $this->session->userdata('user_type');
        $user_ID = $this->session->userdata('userID');
        $data['proj_update'] = $this->prop->get_props_update_single($updID);
        $data['prop'] = $this->prop->get_oneProp($data['proj_update']['projID']);
        
        if ( $usertype == 'admin' || ( $usertype == 'builder' && $user_ID == $data['prop']['userID'] ) ) {
        
            if ( $_POST ) {
                if ( !empty($_POST['update_descr']) ) {
                    $new_updates = array();
                    $new_updates['descr'] = $_POST['update_descr'];
                    if ( !empty( $_POST['update_img'] ) ) $new_updates['images'] = implode(',',$_POST['update_img']);
                    $this->prop->update_prop_updates( $updID, $new_updates );
                } else
                if ( !empty( $_POST['update_img'] ) ) {
                    $new_updates = array();
                    $new_updates['images'] = implode(',',$_POST['update_img']);
                    $this->prop->update_prop_updates( $updID, $new_updates );
                }
                $this->prop->delete_unused_proj_images();
            }
            
            $data['proj_update'] = $this->prop->get_props_update_single($updID);
            $data['prop'] = $this->prop->get_oneProp($data['proj_update']['projID']);
            $data['proj_update_images'] = $this->prop->get_prop_images($data['proj_update']['images']);
            $this->load->view('editPropUpdate', $data);
            
        } else {
            show_404();
        }
    }
	
	function deletePropUpdate($updID){
        $proj_update = $this->prop->get_props_update_single($updID);
        $projID = $proj_update['projID'];
            
        if ( $usertype == 'admin' || $usertype == 'builder' ) {
            $this->prop->delete_proj_images( explode( ',', $proj_update['images'] ) );
            $this->prop->delete_prop_update($updID);
        }
		redirect(base_url().'props/singleProp/'.$projID);
	}
	
	function deleteProp($propID){
        $proj = $this->prop->get_oneProp($propID);
        
		$this->prop->delete_proj_images( explode( ',', $proj['images'] ) );
		$this->prop->delete_proj_investments($propID);
		$this->prop->delete_project_updates($propID);
		$this->prop->delete_prop($propID);
        
		redirect(base_url().'props/');
	}
	
	function approveProp($propID, $stat){
		if($stat== 1){
			$data=array('active'=>0);  
		}else if($stat== 0){
			$data=array('active'=>1);
		}
		$this->prop->update_props($propID, $data);
		redirect(base_url().'props/');
	}
	
	function about(){
		$this->load->view('about');
	}
    function work(){
		$this->load->view('who_we_work_with');
	}
    function referral(){
		$this->load->view('our_referral_program');
	}


    function upload_prop_image(){
       
        if(isset($_FILES["prop_image"]["name"]))  
	   {  
            if ( !is_dir('uploads') ) {
                mkdir('uploads', 0777, TRUE);
            }
            if ( !is_dir('uploads/'.date('Y')) ) {
                mkdir('uploads/' . date('Y'), 0777, TRUE);
            }
            if ( !is_dir('uploads/'.date('Y').'/'.date('m')) ) {
                mkdir('uploads/' . date('Y').'/'.date('m'), 0777, TRUE);
            }
			$config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';  
			$this->load->library('upload', $config);  
			if(!$this->upload->do_upload('prop_image'))  
			{  
				 echo $this->upload->display_errors();  
			}  
			else  
			{  
				$data = array('upload_data' => $this->upload->data());
                
                $html = '<div class="row propImages">
                    <div class="col-md-3 col-6">
                    <div class="position-relative">
                        <img class="img-fluid" src="'.$data['upload_data']['full_path'].'" alt="">
                        <button type="button" class="btn btn-primary shadow"><i class="far fa-trash-alt"></i></button>
                    </div>                            
                    </div>
                </div>';
  
				echo $html;  
			}  
	    }  
    }
    
    function upload_image (){
        if (!$_FILES) {
            show_404();
        } 
        else {
            if ( !is_array($_FILES['userfile']['type']) ) {
                $file_type = $_FILES['userfile']['type'];
                
                $allowed = array("image/jpeg", "image/gif", "image/png", "image/jpg", "image/bmp");
                if(!in_array($file_type, $allowed)) {
                    $error_message = "Only '.jpg', '.gif', '.bmp', and '.png' files are allowed.";
                    $error_format = 1;
                    echo json_encode(array('status'=>'error','error_message' => $error_message));
                }else
                if ($_FILES) {
                    $img = $this->prop->nd_do_upload();
                    echo json_encode(array('status'=>'success','file_name' => $img));
                }
            } else {
                $allowed = array("image/jpeg", "image/gif", "image/png", "image/jpg", "image/bmp");
                $return = array();
                foreach ( $_FILES['userfile']['type'] as $k=>$index ) {
                    $file_type = $_FILES['userfile']['type'][$k];
                
                    if(!in_array($file_type, $allowed)) {
                        $error_message = "Only '.jpg', '.gif', '.bmp', and '.png' files are allowed.";
                        $error_format = 1;
                        $return[] = array('status'=>'error','error_message' => $error_message);
                    }
                }
                
                if ( empty($return) && $_FILES ) {
                    $return = $this->prop->nd_do_upload_multi();
                }
                
                echo json_encode(array('success'=>'multi','imgs'=>$return));
                exit();
            }
        }
        
        exit();
    }
	
}


























