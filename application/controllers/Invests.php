<?php

class Invests Extends CI_Controller{
	function __construct() {
        parent::__construct();
		$this->load->model('invest');
		$this->load->helper('form');
	}
	
	function new_invest(){		
		if($_POST){			
		  if($_POST['invest'] >= 10000){$roiPercent= 0.08;}
		  if($_POST['invest'] >= 2500 && $_POST['invest'] < 10000){$roiPercent= 0.09;}
		  if($_POST['invest'] >= 500 && $_POST['invest'] < 2500){$roiPercent= 0.1;}
		  if($_POST['invest'] < 500){$roiPercent= 0.11;}
		  echo $_POST['invest'];
		  if($_POST['refNum']){$refNum= $_POST['refNum'];}else{$refNum= 0;}
		  $data= array(
		  	'userID'=> $this->session->userdata('userID'),
			'projID'=>$_POST['projID'],
			'amt'=>$_POST['invest'],
			'amtFunded'=>$_POST['amtFunded'],//this is not used in update
			'intrest'=>$roiPercent,
			'refNum'=>$refNum,
			'refSwitch'=>1,
			'confirm'=>0
		  );
		  $amtFunded= ($this->invest->check_pledge($data));
		  $this->invest->update_fundedAmt($amtFunded, $_POST['projID']);
		}
	}
	
	function confirm_pledges($projID){
		$this->invest->confirm_pledges($projID);
		redirect(base_url()."props/");
	}
}