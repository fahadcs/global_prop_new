<?php

class Prop extends CI_Model{
	function get_props(){
		$this->db->select()->from('projects')->order_by('date_added','desc');		
		$query= $this->db->get();		
		return $query->result_array(); 		
	}

    function get_prop_front_image($id){
        $this->db->select()->from('images_uploaded')->where(array('project_id'=> $id));		
		$query= $this->db->get();	
        
		return $query->result_array(); 	
    }
	
	function get_oneProp($projID){
		$this->db->select()->from('projects')->where(array('projID'=> $projID))->order_by('date_added', 'desc');	
		$query= $this->db->get();			
		if($query->row('amtFunded')< $query->row('projAmt')){
			//Turn Confirmations Off
			$this->reset_confirmations($projID);
		}
		return $query->first_row('array');
	}
	
	function reset_confirmations($projID){
		$data= array('confirm'=>0);
		$this->db->where('projID', $projID);
		$this->db->update('invest', $data);
	}
	
	function insert_props($data){
		$this->db->insert('projects', $data);
		return $this->db->insert_id();
	}
	
	function update_props($propId, $data){
		$this->db->where('projID', $propId);
		$this->db->update('projects', $data);
	}
	
	function delete_prop($propId){
		$this->db->where('projID', $propId);
		$this->db->delete('projects');
	}
	
	function get_props_count(){
		$this->db->select('projID')->from('projects')->where('active',1);
		$query= $this->db->get();
		return $query->num_rows();
	}
	
    function delete_proj_investments ( $propID ) {
		$this->db->where('projID', $propID);
		$this->db->delete('invest');
    }
    
    function delete_proj_images ( $img_id_list ) {
        $dimensions = array('120','240','480');
        
        $this->db->select()->from('images_uploaded')->where_in('imgID',$img_id_list);
        $query = $this->db->get();
        $images = $query->result_array();
        
        foreach ($images as $image) {
            $raw_name = $image['raw_name'];
            $full_path = $image['full_path'];
            unlink($full_path);
            foreach ( $dimensions as $dim ) {
                unlink( str_replace( $raw_name, $raw_name.'_'.$dim, $full_path ) );
            }
        }
        
        $this->db->where_in('imgID', $img_id_list);
		$this->db->delete('images_uploaded');
    }
    
    function delete_unused_proj_images () {
        $images_in_use = array();
        
        $this->db->select()->from('projects_update');
        $query = $this->db->get();
        $updates = $query->result_array();
        foreach ( $updates as $upd ) {
            if ( !empty($upd['images']) ) $images_in_use[] = $upd['images'];
        }
        
        $this->db->select()->from('projects');
        $query = $this->db->get();
        $projects = $query->result_array();
        foreach ( $projects as $proj ) {
            if ( !empty($proj['images']) ) $images_in_use[] = $proj['images'];
        }
        
        if ( !empty( $images_in_use ) ) {
            $images_in_use_str = implode( ',', $images_in_use );
            $images_in_use = explode( ',', $images_in_use_str );
            
            $this->db->select()->from('images_uploaded')->where('imgID NOT IN ("'.implode('","', $images_in_use).'")');
            $query = $this->db->get();
            $row_images_not_in_use = $query->result_array();
            
            if ( !empty($row_images_not_in_use) ) {
                $delete_images = array();
                foreach ($row_images_not_in_use as $img_delete) {
                    $delete_images[] = $img_delete['imgID'];
                }
                $this->delete_proj_images( $delete_images );
            }
        }
    }
    
    function delete_project_updates ( $projID ) {
        
        $this->db->select()->from('projects_update')->where_in('projID',$img_id_list);
        $query = $this->db->get();
        $updates = $query->result_array();
        
        foreach ( $updates as $update ) {
            $this->delete_proj_images( explode( ',', $update['images'] ) );
        }
        
        $this->db->where_in('projID', $projID);
		$this->db->delete('projects_update');
    }
	
	function delete_prop_update($updId){
		$this->db->where('updateID', $updId);
		$this->db->delete('projects_update');
	}
    
	function insert_prop_update($data){
		$this->db->insert('projects_update', $data);
		return $this->db->insert_id();
	}
    
	function update_prop_updates($updID, $data){
		$this->db->where('updateID', $updID);
		$this->db->update('projects_update', $data);
	}
    
    function get_props_update($propID, $limit = 0){
		$this->db->select()->from('projects_update')->where('projID',$propID)->order_by('date_added', 'desc');
        if ( $limit != 0 ) {
            $this->db->limit($limit);
        }        
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function get_props_update_single($updID){
		$this->db->select()->from('projects_update')->where('updateID',$updID);	
		$query = $this->db->get();
		return $query->first_row('array');
    }
	
	function get_prop_images($id_list){
		$this->db->select()->from('images_uploaded')->where_in('imgID',explode(',',$id_list));		
		$query= $this->db->get();
		return $query->result_array();
	}
    
    function nd_do_upload(){
        
        if ( !is_dir('uploads') ) {
            mkdir('uploads', 0777, TRUE);
        }
        if ( !is_dir('uploads/'.date('Y')) ) {
            mkdir('uploads/' . date('Y'), 0777, TRUE);
        }
        if ( !is_dir('uploads/'.date('Y').'/'.date('m')) ) {
            mkdir('uploads/' . date('Y').'/'.date('m'), 0777, TRUE);
        }
        
		$config = array(
			'allowed_types' => 'jpg|jpeg|gif|png',
			'encrypt_name' => true,
			'upload_path' => 'uploads/'.date('Y').'/'.date('m').'/',
			'max_size' => 10000
		);
		
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
			return $error;
		}
        
		$image_data = $this->upload->data();
        
        // load image lib
        $this->load->library('image_lib');
        
        // 120
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image'  => $config['upload_path'].''.$image_data['file_name'],
            'new_image'     => $config['upload_path'].''.$image_data['file_name'],
            'maintain_ratio'=> TRUE ,
            'create_thumb'  => TRUE ,
            'thumb_marker'  => '_120' ,
            'width'         => 120,
            'height'        => 120 
        );
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
        
        // 240
        $config_manip['thumb_marker'] = '_240';
        $config_manip['width'] = 240;
        $config_manip['height'] = 240;
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
        
        // 480
        $config_manip['thumb_marker'] = '_480';
        $config_manip['width'] = 480;
        $config_manip['height'] = 480;
        $this->image_lib->initialize($config_manip);
        $this->image_lib->resize();
        
        $this->db->insert(
            'images_uploaded', 
            array(
                'file_name' => $image_data['file_name'],
                'raw_name'  => $image_data['raw_name'],
                'file_ext'  => $image_data['file_ext'],
                'full_path' => $config['upload_path'].''.$image_data['file_name']
            )
        );
		
		// return $image_data;
		return $this->db->insert_id();
    }
    
    function nd_do_upload_multi(){
        
        if ( !is_dir('uploads') ) {
            mkdir('uploads', 0777, TRUE);
        }
        if ( !is_dir('uploads/'.date('Y')) ) {
            mkdir('uploads/' . date('Y'), 0777, TRUE);
        }
        if ( !is_dir('uploads/'.date('Y').'/'.date('m')) ) {
            mkdir('uploads/' . date('Y').'/'.date('m'), 0777, TRUE);
        }
        
		$config = array(
			'allowed_types' => 'jpg|jpeg|gif|png',
			'encrypt_name' => true,
			'upload_path' => 'uploads/'.date('Y').'/'.date('m').'/',
			'max_size' => 10000
		);
		
		$this->load->library('upload');
        
        // load image lib
        $this->load->library('image_lib');
        
        // resize config
        $config_manip = array(
            'image_library' => 'gd2',
            'maintain_ratio'=> TRUE ,
            'create_thumb'  => TRUE
        );
        
        $errors = array();
        $uploaded_images = array();
        foreach ( $_FILES['userfile']['name'] as $i=>$file ) {
            $_FILES['uploadfile']['name']       = $_FILES['userfile']['name'][$i];
            $_FILES['uploadfile']['type']       = $_FILES['userfile']['type'][$i];
            $_FILES['uploadfile']['tmp_name']   = $_FILES['userfile']['tmp_name'][$i];
            $_FILES['uploadfile']['error']      = $_FILES['userfile']['error'][$i];
            $_FILES['uploadfile']['size']       = $_FILES['userfile']['size'][$i];
            
            $this->upload->initialize($config);
            
            if(!$this->upload->do_upload('uploadfile')){
                $uploaded_images[] = array('error' => $this->upload->display_errors());
                continue;
            }
            $upload_data = $this->upload->data();
            
            // 120
            $config_manip['source_image'] = $config['upload_path'].''.$upload_data['file_name'];
            $config_manip['new_image'] = $config['upload_path'].''.$upload_data['file_name'];
            $config_manip['thumb_marker'] = '_120';
            $config_manip['width'] = 120;
            $config_manip['height'] = 120;
            $this->image_lib->initialize($config_manip);
            $this->image_lib->resize();
            
            // 240
            $config_manip['thumb_marker'] = '_240';
            $config_manip['width'] = 240;
            $config_manip['height'] = 240;
            $this->image_lib->initialize($config_manip);
            $this->image_lib->resize();
            
            // 480
            $config_manip['thumb_marker'] = '_480';
            $config_manip['width'] = 480;
            $config_manip['height'] = 480;
            $this->image_lib->initialize($config_manip);
                $this->image_lib->resize();
            
            $this->db->insert(
                'images_uploaded', 
                array(
                    'file_name' => $upload_data['file_name'],
                    'raw_name'  => $upload_data['raw_name'],
                    'file_ext'  => $upload_data['file_ext'],
                    'full_path' => $config['upload_path'].''.$upload_data['file_name']
                )
            );
            
            $uploaded_images[] = $this->db->insert_id();
        }
		
		// return $image_data;
		return $uploaded_images;
    }
}