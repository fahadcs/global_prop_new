<?php
class Invest extends CI_Model{
	function check_pledge($data){		
		$newAmount= $data['amt'];//then add this to project amtFunded		
		$whereArray= array('userID'=>$data['userID'], 'projID'=>$data['projID'],);
		$this->db->select('amt')->from('invest')->where($whereArray);					
		$query2= $this->db->get();		
		$insertArray= array(
				'userID'=> $data['userID'],
				'projID'=>$data['projID'],
				'amt'=>$data['amt'],
				'intrest'=>$data['intrest'],
				'reffNum'=>$data['refNum'],
				'refSwitch'=>$data['refSwitch'],
				'confirm'=>$data['confirm'],
			);		
		if($query2->num_rows()>0){
			$this->db->where($whereArray);
			$this->db->update('invest', $insertArray);
			$oldAmount= $query2->row('amt'); //subtract this from project amtFunded			
		}else{		
			$oldAmount= 0;	
			$query3= $this->db->insert('invest', $insertArray);
		}			
		$newFundedAmt= $data['amtFunded']-$oldAmount+$newAmount;
		return $newFundedAmt;
	}
	
	function update_fundedAmt($amtFunded, $projID ){
		$data= array('amtFunded'=>$amtFunded);
		$this->db->where('projID', $projID);
		$this->db->update('projects', $data);
		redirect(base_url().'props/singleProp/'.$projID);
	}
	
	function get_singleInvst($projID){
		$whereArray= array('projID'=>$projID, 'userID'=> $this->session->userdata('userID'));
		$this->db->select('amt, confirm')->from('invest')->where($whereArray);
		$query= $this->db->get();
		if($query->num_rows()>0){return $query->row_array();}else{return 0;}		
	}
	
	function get_allInvest($projID){
		$this->db->select()->from('invest')->where('projID', $projID);
		$this->db->join('users', 'invest.userID = users.userID');		
		$query= $this->db->get();
		if($query->num_rows()>0){
			 return $query->result_array();
		}
	}
	
	//get_referrals function -> called from props controler / singlProp function to the oneProp.php view
	function get_referrals($projID, $refID, $sessUserAmt){		
		$whereArray= array(
			'projID'=>$projID,
			'reffNum'=>$refID,
		);
		$this->db->select('amt')->from('invest')->where($whereArray);
		$query= $this->db->get();
		if($query->num_rows()>0){			
			
			$total= 0;
			foreach($query->result_array() as $row){
				$total+= $row['amt'];
			}
			
			//calculate any added interest from the referral totals and update investors ROI field
			if($total >= 10000){
				//check correct extra ROI based on total rewards amount
				if($total >= 10000 && $total < 20000){$extraROI= 0.01;}
				if($total >= 20000 && $total < 30000){$extraROI= 0.02;}
				if($total >= 30000 && $total < 40000){$extraROI= 0.03;}
				if($total >= 40000 ){$extraROI= 0.04;}
				
				//check for correct base RIO based on session user's current investment
				if($sessUserAmt >= 10000){$baseROI= 0.08;}
				if($sessUserAmt >= 2500 && $sessUserAmt < 10000){$baseROI= 0.09;}
				if($sessUserAmt >= 500 && $sessUserAmt < 2500){$baseROI= 0.1;}
				if($sessUserAmt < 500){$baseROI= 0.11;}
				
				//add $baseROI and $extraROI
				$totalROI= $baseROI + $extraROI;
				
				//run an ROI update function
				$this->update_roi($projID, $totalROI);
			}
			
			return $total;
		}		
	}
	
	function update_roi($projID,$totalROI){
		$data= array('intrest'=>$totalROI,);
		$whereArray= array(
			'userID'=>$this->session->userdata('userID'),
			'projID'=>$projID,
		);
		$this->db->select('intrest')->from('invest')->where($whereArray);
		$query= $this->db->get();
		if($query->row('intrest') < 0.13 || $totalROI < .13){
			$this->db->where($whereArray);
			$this->db->update('invest', $data);
			
		}
	}
	
	function confirm_pledges($projID){
		$whereAry= array('projID'=>$projID, 'userID'=>$this->session->userdata('userID'),);
		$data= array('confirm'=>1,);
		$this->db->where($whereAry);
		$this->db->update('invest', $data);
	}
	
}
































