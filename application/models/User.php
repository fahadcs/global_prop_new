<?php

class User Extends CI_Model{	
	
	function create_user($data){		
		$query= $this->db->insert('users', $data);
		return $this->db->insert_id();
	}
	
	function login($username, $password){
		$where=array(
			'username'=>$username,
			'password'=>sha1($password)
		);
		$this->db->select()->from('users')->where($where);
		$query= $this->db->get();
		return $query->first_row('array');
	}
	
	function super_login($userID){
		$this->db->select()->from('users')->where('userID', $userID);
		$query= $this->db->get();
		return $query->first_row('array');
	}
	
	function update_user($data){
		$this->db->where('userID', $this->session->userdata('userID'));
		$this->db->update('users', $data);
	}
	
	function get_allUsers($type){
		$this->db->select()->from('users')->where('user_type', $type)->order_by('username', 'desc');
		$query= $this->db->get();
		return $query->result_array();
	}
	
	function get_bldrProjects($userID){
		$this->db->select()->from('projects')->where('userID', $userID)->order_by('title', 'desc');
		$query= $this->db->get();
		return $query->result_array();
	}
	
	function get_pledges($userID){
		$this->db->select('invest.userID, invest.projID, amt, intrest, reffNum, title, active')->
				   from('invest')->
				   where('invest.userID', $userID);
		$this->db->join('projects', 'invest.projID = projects.projID');
		$query= $this->db->get();
		return $query->result_array();		
	}
	
	
	
}

?>